import 'package:instantiator/instantiator.dart';

abstract class TestClass {}

TestClass f() => TestClassImpl();

@Instance(of: TestClass, permanent: true, environments: ['dev', 'prod'])
class TestClassImpl implements TestClass {}

@Instance()
class Test {}

@LazyInstance()
class SelfService {}

abstract class ServiceB {}

@AsyncInstance(of: ServiceB)
abstract class ServiceBImpl {}

@Instance()
class Clazz {
  final Test test;
  final bool fx;

  Clazz({
    @Param() this.test,
    this.fx,
  });
}
