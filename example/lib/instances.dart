import 'package:get/get.dart';
import 'package:example/instance_example.dart';
import 'package:example/src/test2.dart';
import 'package:example/test.dart';
import 'package:example/src/ext.dart';

class InstanceConfig {

	void configDev(){
		Get.put<TestClass>(TestClassImpl(), permanent: true,);
	}

	void configProd(){
		Get.put<TestClass>(TestClassImpl(), permanent: true,);
	}

	void configDefault(){
		Get.put<Test>(Test());
		Get.lazyPut<SelfService>(() => SelfService());
		Get.putAsync<ServiceB>(() async => ServiceBImpl());
		Get.put<Clazz>(Clazz(test: Get.find<Test>()));
		Get.put<Test3>(Test3());
		Get.put<Test2>(Test2());
		Get.put<PExterno>(TestExt());
	}

}

