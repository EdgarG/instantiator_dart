class Instance {
  final Type of;
  final List<String> environments;
  final bool permanent;

  const Instance({
    this.of,
    this.environments,
    this.permanent = false,
  });
}

class LazyInstance {
  final Type of;
  final List<String> environments;
  final bool fenix;

  const LazyInstance({
    this.of,
    this.environments,
    this.fenix = false,
  });
}

class AsyncInstance {
  final Type of;
  final List<String> environments;
  final bool permanent;

  const AsyncInstance({
    this.of,
    this.environments,
    this.permanent = false,
  });
}

class Param {
  const Param();
}