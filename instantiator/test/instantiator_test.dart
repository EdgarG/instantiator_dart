import 'package:instantiator/instantiator.dart';
import 'package:test/test.dart';

void main() {
  group('Instance test', () {

    // test('Type (of) must not be null', (){
    //   expect(() => Instance(), throwsA(TypeMatcher<AssertionError>()));
    //   //expect(() => Instance(builder: () => null), throwsA(TypeMatcher<AssertionError>()));
    // });


    test('Does not need enviroments', (){
      final instance = Instance(of: String);
      // final instance = Instance(of: String, builder: () => null);
      expect(instance.environments, null);
    });

  });
}
