import 'dart:async';
import 'dart:convert';
import 'dart:ffi';
import 'package:analyzer/dart/element/element.dart';
import 'package:instantiator/instantiator.dart';

import 'package:glob/glob.dart';
import 'package:build/build.dart';
import 'package:build/src/builder/build_step.dart';
import 'package:path/path.dart' as p;
import 'package:source_gen/source_gen.dart';

const TypeChecker _instanceChecker = TypeChecker.fromRuntime(Instance);
const TypeChecker _lazyInstanceChecker = TypeChecker.fromRuntime(LazyInstance);
const TypeChecker _asyncInstanceChecker =
    TypeChecker.fromRuntime(AsyncInstance);

const TypeChecker _paramChecker = TypeChecker.fromRuntime(Param);

class _InstanceEnv {
  final String environment;
  final String content;
  _InstanceEnv(this.environment, this.content);
}

class _Param {
  final String path;
  final String content;

  _Param(this.path, this.content);
}

class _OfClassName {
  final String name;
  final String path;

  _OfClassName(this.name, this.path);
}

class _InstanceToWrite {
  final List<String> imports;
  final String content;

  _InstanceToWrite(this.imports, this.content);
}

class _File {
  List<String> imports;
  List<_InstanceEnv> instances;
  Map<String, List<_InstanceEnv>> environments;

  _File() {
    imports = [];
    instances = [];
    environments = Map();
  }
}

class InstancesBuilder extends Builder {
  // Glob of all input files
  static final inputFiles = new Glob('lib/**.dart');

  /*@override
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) async {
    var libs = await buildStep.findAssets(inputFiles);
    print(libs);

    return "//InstanceGenerator";
  }*/

  @override
  Map<String, List<String>> get buildExtensions {
    // Using r'...' is a "raw" string, so we don't interpret $lib as a field.
    // An alternative is escaping manually, or '\$lib'.
    return const {
      r'$lib$': const ['instances.dart']
    };
  }

  @override
  FutureOr<void> build(BuildStep buildStep) async {
    var gFile = _File();

    await for (final input in buildStep.findAssets(inputFiles)) {
      var assetId = AssetId(buildStep.inputId.package, input.path);
      //final content = await buildStep.readAsString(assetId);
      final isLibrary = await buildStep.resolver.isLibrary(assetId);

      if (isLibrary) {
        final lib = await buildStep.resolver.libraryFor(assetId);
        final libReader = LibraryReader(lib);
        final file = await generate(libReader, buildStep);

        file.imports.forEach((element) {
          if (!gFile.imports.contains(element)) {
            gFile.imports.add(element);
          }
        });

        file.instances.forEach((instance) {
          if(!gFile.environments.containsKey(instance.environment)){
            gFile.environments.putIfAbsent(instance.environment, () => <_InstanceEnv>[]);
          }
          //gFile.environments[instance.environment].writeln();
          gFile.environments[instance.environment].add(instance);
        });

      }
    }

    String fileContent = _parseFile(gFile);

    /// Write to the file
    final outputFile =
        AssetId(buildStep.inputId.package, p.join('lib', 'instances.dart'));

    await buildStep.writeAsString(outputFile, fileContent);
  }

  String _parseFile(_File file) {
    var buffer = StringBuffer();

    buffer.writeln("import 'package:get/get.dart';");
    buffer.writeln(file.imports.map((e) => "import '$e';").join("\n"));

    buffer.writeln("\nclass InstanceConfig {\n");

    file.environments.forEach((env, instances) {
      buffer.writeln("\tFuture<void> config${env[0].toUpperCase() + env.substring(1)}() async {");
      instances.forEach((inst) {
        buffer.writeln("\t\t${inst.content}");
      });
      buffer.writeln("\t}\n");
    });

    buffer.writeln("}\n");

    // buffer.writeln(extensionBuffer.toString());

    //buffer.writeln(file.toEncodedJson());

    return buffer.toString();
  }

  @override
  FutureOr<_File> generate(LibraryReader library, BuildStep buildStep) {
    _File file = _File();

    // var extensionBuffer = StringBuffer();

    for (final _class in library.classes) {
      // _writeExtension(extensionBuffer, className);
      var reader;
      _InstanceToWrite instance;
      var ofPath;

      if (_isInstanceAnnotated(_class)) {
        reader = _getInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        ofPath = ofClassName.path;
        final permanent = reader.peek("permanent");
        final isPermanent = permanent != null && permanent.boolValue == true;
        instance = _writeInstance(_class, ofClassName.name, isPermanent);
      } else if (_isLazyInstanceAnnotated(_class)) {
        reader = _getLazyInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        ofPath = ofClassName.path;
        final fenix = reader.peek("fenix");
        final isFenix = fenix != null && fenix.boolValue == true;
        instance = _writeLazyInstance(_class, ofClassName.name, isFenix);
      } else if (_isAsyncInstanceAnnotated(_class)) {
        reader = _getAsyncInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        ofPath = ofClassName.path;
        final permanent = reader.peek("permanent");
        final isPermanent = permanent != null && permanent.boolValue == true;
        instance = _writeAsyncInstance(_class, ofClassName.name, isPermanent);
      }

      if(reader != null) {
        var path = "package:${_class.source.uri.path}";
        if (!file.imports.contains(path)) {
          file.imports.add(path);
        }

        if(ofPath != null){
          path = "package:${ofPath}";

          if (!file.imports.contains(path)) {
            file.imports.add(path);
          }
        }

        instance.imports.forEach((path) {
          path = "package:${path}";
          if (!file.imports.contains(path)) {
            file.imports.add(path);
          }
        });

        final envs = _getEnvironments(reader);

        envs.forEach((env) {
          var inst = _InstanceEnv(env, instance.content);
          file.instances.add(inst);
        });
      }
    }

    return file;
  }

  List<String> _getEnvironments(ConstantReader reader) {
    final envs = reader.peek('environments');

    if (envs == null) {
      return ['default'];
    }

    return envs.listValue.map((e) => e.toStringValue()).toList();
  }

  List<String> _addEnviromentsIfNeeded(
      ConstantReader reader, Map<String, StringBuffer> environments) {
    final envs = reader.peek('environments');

    if (envs == null) {
      environments.putIfAbsent('default', () => StringBuffer());
      return ['default'];
    }

    return envs.listValue.map((e) {
      final name = e.toStringValue();
      environments.putIfAbsent(name, () => StringBuffer());
      return name;
    }).toList();
  }

  _InstanceToWrite _writeInstance(
      ClassElement _class, String ofClassName, bool isPermanent) {
    final className = _class.displayName;
    final params = _getParams(_class);
    var content;
    var parameters = params.map((p) => p.content).toList().join(",");
    if (isPermanent) {
      content = "Get.put<${ofClassName}>($className(${parameters}), permanent: true,);";
    }else{
      content = "Get.put<${ofClassName}>($className(${parameters}));";
    }

    return _InstanceToWrite(params.map((p) => p.path).toList(), content);
  }

  _InstanceToWrite _writeLazyInstance(
      ClassElement _class, String ofClassName, bool isFenix) {
    final className = _class.displayName;
    final params = _getParams(_class);
    var content;
    var parameters = params.map((p) => p.content).toList().join(",");

    if (isFenix) {
      content = "Get.lazyPut<${ofClassName}>(() => $className(${parameters}), fenix: true,);";
    }else {
      content = "Get.lazyPut<${ofClassName}>(() => $className(${parameters}));";
    }
    return _InstanceToWrite(params.map((p) => p.path).toList(), content);
  }

  _InstanceToWrite _writeAsyncInstance(
      ClassElement _class, String ofClassName, bool isPermanent) {
    final className = _class.displayName;
    final params = _getParams(_class);
    var content;
    var parameters = params.map((p) => p.content).toList().join(",");

    if (isPermanent) {
      content = "Get.putAsync<${ofClassName}>(() async => $className(${parameters}), permanent: true,);";
    }else{
      content = "Get.putAsync<${ofClassName}>(() async => $className(${parameters}));";
    }
    return _InstanceToWrite(params.map((p) => p.path).toList(), content);
  }

  List<_Param> _getParams(ClassElement _class) {
    final constructors = _class.constructors;
    List<_Param> list = [];

    if (constructors != null && constructors.isNotEmpty) {
      constructors.forEach((constructor) {
        constructor.parameters.forEach((parameter) {
          if (parameter.metadata != null) {
            final param = parameter.metadata.firstWhere(
                    (element) => _isParam(element.element),
                orElse: () => null);

            if (param != null) {
              final type = parameter.declaration.type;
              final typeName = type.getDisplayString(withNullability: false);
              var path = type.element.source.uri.path;
              var content;

              if (parameter.isNamed) {
                content = '${parameter.name}: Get.find<${typeName}>()';
              } else if (parameter.isPositional) {
                content = 'Get.find<${typeName}>()';
              }

              list.add(_Param(path, content));
            }
          }
        });
      });
    }

    return list;
  }

  ConstantReader _getInstanceReader(ClassElement element) {
    final annotation =
        _instanceChecker.firstAnnotationOf(element, throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  ConstantReader _getLazyInstanceReader(ClassElement element) {
    final annotation = _lazyInstanceChecker.firstAnnotationOf(element,
        throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  ConstantReader _getAsyncInstanceReader(ClassElement element) {
    final annotation = _asyncInstanceChecker.firstAnnotationOf(element,
        throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  _OfClassName _getOfClassNameFromConstantReader(
      Element element, ConstantReader reader) {
    var name = element.displayName;
    var path = null;

    if (reader.peek("of") != null) {
      var type = reader
          .peek("of")
          .objectValue
          .toTypeValue();

      name = type.getDisplayString(withNullability: false);
      path = type.element.source.uri.path;
    }

    return _OfClassName(name, path);
  }

  bool _isInstanceAnnotated(Element element) {
    return _instanceChecker.hasAnnotationOf(element, throwOnUnresolved: false);
  }

  bool _isLazyInstanceAnnotated(Element element) {
    return _lazyInstanceChecker.hasAnnotationOf(element,
        throwOnUnresolved: false);
  }

  bool _isAsyncInstanceAnnotated(Element element) {
    return _asyncInstanceChecker.hasAnnotationOf(element,
        throwOnUnresolved: false);
  }

  bool _isParam(Element element) {
    return element.getDisplayString(withNullability: false).split(' ')[0] ==
        '$Param';
    //return _paramChecker.hasAnnotationOfExact(element, throwOnUnresolved: false);
  }
}
