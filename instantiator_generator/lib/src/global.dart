import 'dart:async';
import 'dart:convert';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'package:instantiator/instantiator.dart';

const TypeChecker _instanceChecker = TypeChecker.fromRuntime(Instance);
const TypeChecker _lazyInstanceChecker = TypeChecker.fromRuntime(LazyInstance);
const TypeChecker _asyncInstanceChecker =
    TypeChecker.fromRuntime(AsyncInstance);

const TypeChecker _paramChecker = TypeChecker.fromRuntime(Param);

class _File {
  List<String> imports;
  Map<String, StringBuffer> environments;

  _File(){
    imports = [];
    environments = Map();
  }

  String toEncodedJson(){
    Map<String, dynamic> object = {
      "imports": imports,
      "environments": environments.entries.map((e){
        return {
          e.key: e.value.toString()
        };
      }).toList(),
    };

    return jsonEncode(object);
  }

}

class GlobalGenerator extends Generator {
  @override
  FutureOr<String> generate(LibraryReader library, BuildStep buildStep) {
    _File file = _File();

    // var extensionBuffer = StringBuffer();

    for (final _class in library.classes) {
      // _writeExtension(extensionBuffer, className);
      var path = "package:${_class.source.uri.path}";
      if(!file.imports.contains(path)){
        file.imports.add(path);
      }

      if (_isInstance(_class)) {
        final reader = _getInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        final permanent = reader.peek("permanent");
        final isPermanent = permanent != null && permanent.boolValue == true;

        final envs = _addEnviromentsIfNeeded(reader, file.environments);

        envs.forEach((env) {
          var buffer = file.environments[env];
          _writeInstance(buffer, _class, ofClassName, isPermanent);
        });
      } else if (_isLazyInstance(_class)) {
        final reader = _getLazyInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        final fenix = reader.peek("fenix");
        final isFenix = fenix != null && fenix.boolValue == true;

        final envs = _addEnviromentsIfNeeded(reader, file.environments);

        envs.forEach((env) {
          var buffer = file.environments[env];
          _writeLazyInstance(buffer, _class, ofClassName, isFenix);
        });
      } else if (_isAsyncInstance(_class)) {
        final reader = _getAsyncInstanceReader(_class);
        final ofClassName = _getOfClassNameFromConstantReader(_class, reader);
        final permanent = reader.peek("permanent");
        final isPermanent = permanent != null && permanent.boolValue == true;

        final envs = _addEnviromentsIfNeeded(reader, file.environments);

        envs.forEach((env) {
          var buffer = file.environments[env];
          _writeAsyncInstance(buffer, _class, ofClassName, isPermanent);
        });
      }
    }

    var buffer = StringBuffer();
    /*
    buffer.writeln("import 'package:get/get.dart';\n\n");
    buffer.writeln(imports.join("\n"));

    buffer.writeln("class InstanceConfig {\n");

    environments.forEach((env, envBuffer) {
      buffer.writeln(
          "\tvoid config${env[0].toUpperCase() + env.substring(1)}(){\n\n");
      buffer.writeln("\t\t${envBuffer.toString()}");
      buffer.writeln("\n\t}\n");
    });

    buffer.writeln("\n}\n");

    // buffer.writeln(extensionBuffer.toString());
    */
    //buffer.writeln(file.toEncodedJson());

    return buffer.toString();
  }

  // void _writeExtension(StringBuffer buffer, String className){
  //   buffer.writeln('extension \$${className} on ${className} {');
  //   buffer.writeln("\tfactory $className.instance(){\n");
  //   buffer.writeln("\t\treturn Get.find<$className>();");
  //   buffer.writeln("\n\t}\n");
  //   buffer.writeln("}\n");
  // }

  List<String> _addEnviromentsIfNeeded(
      ConstantReader reader, Map<String, StringBuffer> environments) {
    final envs = reader.peek('environments');

    if (envs == null) {
      environments.putIfAbsent('default', () => StringBuffer());
      return ['default'];
    }

    return envs.listValue.map((e) {
      final name = e.toStringValue();
      environments.putIfAbsent(name, () => StringBuffer());
      return name;
    }).toList();
  }

  void _writeInstance(StringBuffer buffer, ClassElement _class,
      String ofClassName, bool isPermanent) {
    final className = _class.displayName;
    final params = _getParams(_class);

    if (isPermanent) {
      buffer.writeln(
          "\t\tGet.put<${ofClassName}>($className(${params.join(",")}), permanent: true,);");
    } else {
      buffer.writeln("\t\tGet.put<${ofClassName}>($className(${params.join(",")}));");
    }
  }

  List<String> _getParams(ClassElement _class){
    final constructors = _class.constructors;
    List<String> list = [];

    if (constructors != null && constructors.isNotEmpty) {
      constructors.forEach((constructor) {
        constructor.parameters.forEach((parameter) {

          if (parameter.metadata != null) {
            final param = parameter.metadata.firstWhere(
                    (element) => _isParam(element.element),
                orElse: () => null);

            if (param != null) {
              final type = parameter.declaration.type.getDisplayString(withNullability: false);
              if(parameter.isNamed) {
                list.add('${parameter.name}: Get.find<${type}>()');
              }else if(parameter.isPositional){
                list.add('Get.find<${type}>()');
              }
            }
          }
        });
      });
    }

    return list;
  }

  void _writeLazyInstance(StringBuffer buffer, ClassElement _class,
      String ofClassName, bool isFenix) {
    final className = _class.displayName;
    if (isFenix) {
      buffer.writeln(
          "\t\tGet.lazyPut<${ofClassName}>(() => $className(), fenix: true,);");
    } else {
      buffer.writeln("\t\tGet.lazyPut<${ofClassName}>(() => $className());");
    }
  }

  void _writeAsyncInstance(StringBuffer buffer, ClassElement _class,
      String ofClassName, bool isPermanent) {
    final className = _class.displayName;
    if (isPermanent) {
      buffer.writeln(
          "\t\tGet.putAsync<${ofClassName}>(() async => $className(), permanent: true,);");
    } else {
      buffer.writeln(
          "\t\tGet.putAsync<${ofClassName}>(() async => $className());");
    }
  }

  ConstantReader _getInstanceReader(ClassElement element) {
    final annotation =
        _instanceChecker.firstAnnotationOf(element, throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  ConstantReader _getLazyInstanceReader(ClassElement element) {
    final annotation = _lazyInstanceChecker.firstAnnotationOf(element,
        throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  ConstantReader _getAsyncInstanceReader(ClassElement element) {
    final annotation = _asyncInstanceChecker.firstAnnotationOf(element,
        throwOnUnresolved: false);
    return ConstantReader(annotation);
  }

  String _getOfClassNameFromConstantReader(
      Element element, ConstantReader reader) {
    var ofClassName = element.displayName;

    if (reader.peek("of") != null) {
      ofClassName = reader
          .peek("of")
          .objectValue
          .toTypeValue()
          .getDisplayString(withNullability: false);
    }

    return ofClassName;
  }

  bool _isInstance(Element element) {
    return _instanceChecker.hasAnnotationOf(element, throwOnUnresolved: false);
  }

  bool _isLazyInstance(Element element) {
    return _lazyInstanceChecker.hasAnnotationOf(element,
        throwOnUnresolved: false);
  }

  bool _isAsyncInstance(Element element) {
    return _asyncInstanceChecker.hasAnnotationOf(element,
        throwOnUnresolved: false);
  }

  bool _isParam(Element element) {
    return element.getDisplayString(withNullability: false).split(' ')[0] == '$Param';
    //return _paramChecker.hasAnnotationOfExact(element, throwOnUnresolved: false);
  }
}
