import 'package:build/build.dart';
import 'package:instantiator/instantiator.dart';
import 'package:instantiator_generator/src/global.dart';
import 'package:source_gen/source_gen.dart';
import 'package:instantiator_generator/src/instance.dart';

/// Support for doing something awesome.
///
/// More dartdocs go here.
// library intantiator_generator;
//
// export 'src/intantiator_generator_base.dart';

Builder instanceBuilder(BuilderOptions options) => InstancesBuilder();
    //LibraryBuilder(InstanceGenerator(), generatedExtension: ".instance.dart");

Builder globalBuilder(BuilderOptions options) =>
    LibraryBuilder(GlobalGenerator(), generatedExtension: ".global.json");